# Manejador de proyectos
## Link del servidor web de Heroku

https://proyectoreto.herokuapp.com/


![Diagrama](data/Scrum_Management_Proyect.jpeg)
![Tablas](data/ProyectoRetoModelos.png)


Para correr la app se debe introducir lo siguiente:
```
DEBUG=proyecto-reto:* npm start
```
Corre en el puerto 3000

Y para generar la imagen de docker:
```
docker build -t <nombre-a-elegir-para-imagen>
```
Para correr el contenedor:
```
docker run -dti -p3000:3000 --name=<nombre-a-asignar-para-contenedor> <nombre-a-elegir-para-imagen>
```
Para ver el contenedor corriendo
```
docker ps
```
Corre en el puerto 3000, en el navegador:localhost:3000
Para detener el contenedor:
```
docker stop <nombre-a-asignar-para-contenedor>
```
Saludos desde el Himalaya
