import { defineAbility } from '@casl/ability';

export default defineAbility((can, cannot) => {
  can('read', 'all');
  can('delete', 'all');
  can('create', 'all');
});
