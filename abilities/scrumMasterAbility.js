import { defineAbility } from '@casl/ability';

export default defineAbility((can, cannot) => {
  can('read', 'all');
  can('create', 'all');
  can('delete', 'Storie');
  cannot('delete', 'Proyect');
});
