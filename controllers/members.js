const express = require('express');

const Member = require('../models/member');

//aqui se pone 'members' para los mensajes de internacionalizacion

function list(req, res, next) {
    Member.find().then(objs => res.status(200).json({
        message:  res.__('ok.members.list'),
        objs: objs
    })).catch(err => res.status(500).json({
        message: res.__('error.members.list'),
        objs: err
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Member.findOne({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.members.index'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.members.index'),
        objs: err
    }));
}

function create(req, res, next) {
    const fullName = req.body.fullName;
    const birthDate = req.body.birthDate;
    const curp = req.body.curp;
    const rfc = req.body.rfc;
    const address = req.body.address;
    const skills = req.body.skills;
    const permissions = req.body.permissions;

    let member = new Member({
        fullName: fullName,
        birthDate: birthDate,
        curp: curp,
        rfc: rfc,
        address: address,
        skills: skills,
        permissions: permissions
    });

    member.save().then(obj => res.status(200).json({
        message: res.__('ok.members.create'),
        objs: obj
    })).catch(err => res.status(500).json({
        message:res.__('error.members.index'),
        objs: err
    }));
}

async function edit(req, res, next) {
    const id = req.params.id;
    const member = await Member.findOne({'_id':id});

    const fullName = req.body.fullName;
    const birthDate = req.body.birthDate;
    const curp = req.body.curp;
    const rfc = req.body.rfc;
    const address = req.body.address;
    const skills = req.body.skills;
    const permissions = req.body.permissions;

    if(fullName){
        member._fullName = fullName;
    }

    if(birthDate){
        member._birthDate = birthDate;
    }

    if(curp){
        member._curp = curp;
    }

    if(rfc){
        member._rfc = rfc;
    }

    if(address){
        member._address = address;
    }

    if(skills){
      member._skills = skills;
  }

    if(permissions){
    member._permissions = permissions;
}

    member.save().then(obj => res.status(200).json({
        message:res.__('ok.members.edit'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.members.edit'),
        objs: err
    }));
}

function replace(req, res, next) {
    const id = req.params.id;
    const fullName = req.body.fullName ? req.body.fullName : '';
    const birthDate = req.body.birthDate ? req.body.birthDate : '';
    const curp = req.body.curp ? req.body.curp : '';
    const rfc = req.body.rfc ? req.body.rfc : '';
    const address = req.body.address ? req.body.address : '';
    const skills = req.body.skills ? req.body.skills : '';
    const permissions = req.body.number ? req.body.permissions : '';

    let member = new Object({
        _fullName: fullName,
        _birthDate: birthDate,
        _curp: curp,
        _rfc: rfc,
        _address: address,
        _skills: skills,
        _permissions: permissions
    });
    Member.findOneAndReplace({'_id':id}, member).then(obj => res.status(200).json({
        message: res.__('ok.members.replace'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.members.replace'),
        objs: err
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Member.remove({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.members.destroy'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.members.destroy'),
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}
