const express = require('express');

const ProductBacklog = require('../models/productBacklog');

//aqui se pone 'productBacklog' para los mensajes de internacionalizacion

function list(req, res, next) {
    ProductBacklog.find().then(objs => res.status(200).json({
        message:  res.__('ok.productBacklog.list'),
        objs: objs
    })).catch(err => res.status(500).json({
        message: res.__('error.productBacklog.list'),
        objs: err
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    ProductBacklog.findOne({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.productBacklog.index'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.productBacklog.index'),
        objs: err
    }));
}

function create(req, res, next) {
    const features = req.body.features;
    const requirements = req.body.requirements;
    const enhacements = req.body.enhacements;
    const fixes = req.body.fixes;
    const proyect = req.body.proyect;

    let productBacklog = new ProductBacklog({
      features: features,
      requirements: requirements,
      enhacements: enhacements,
      fixes: fixes,
      proyect: proyect
    });

    productBacklog.save().then(obj => res.status(200).json({
        message: res.__('ok.productBacklog.create'),
        objs: obj
    })).catch(err => res.status(500).json({
        message:res.__('error.productBacklog.create'),
        objs: err
    }));
}

async function edit(req, res, next) {
    const id = req.params.id;
    const productBacklog = await ProductBacklog.findOne({'_id':id});

    const features = req.body.features;
    const requirements = req.body.requirements;
    const enhacements = req.body.enhacements;
    const fixes = req.body.fixes;
    const proyect = req.body.proyect;

    if(features){
        productBacklog._features = features;
    }

    if(requirements){
        productBacklog._requirements = requirements;
    }

    if(enhacements){
        productBacklog._enhacements = enhacements;
    }

    if(fixes){
        productBacklog._fixes = fixes;
    }

    if(proyect){
        productBacklog._proyect = proyect;
    }

    productBacklog.save().then(obj => res.status(200).json({
        message:res.__('ok.productBacklog.edit'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.productBacklog.edit'),
        objs: err
    }));
}

function replace(req, res, next) {
    const id = req.params.id;
    const features = req.body.features ? req.body.features : '';
    const requirements = req.body.requirements ? req.body.requirements : '';
    const enhacements = req.body.enhacements ? req.body.enhacements : '';
    const fixes = req.body.fixes ? req.body.fixes : '';
    const proyect = req.body.proyect ? req.body.proyect : '';

    let productBacklog = new Object({
        _fullName: fullName,
        _birthDate: birthDate,
        _curp: curp,
        _rfc: rfc,
        _address: address,
        _skills: skills,
        _permissions: permissions
    });
    ProductBacklog.findOneAndReplace({'_id':id}, productBacklog).then(obj => res.status(200).json({
        message: res.__('ok.productBacklog.replace'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.productBacklog.replace'),
        objs: err
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    ProductBacklog.remove({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.productBacklog.destroy'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.productBacklog.destroy'),
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}
