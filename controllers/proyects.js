const express = require('express');

const Proyect = require('../models/proyect');

//aqui se pone 'proyects' para los mensajes de internacionalizacion

function list(req, res, next) {
  Proyect.find().then(objs => res.status(200).json({
        message:  res.__('ok.proyects.list'),
        objs: objs
    })).catch(err => res.status(500).json({
        message: res.__('error.proyects.list'),
        objs: err
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Proyect.findOne({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.proyects.index'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.proyects.index'),
        objs: err
    }));
}

function create(req, res, next) {
    const name = req.body.name;
    const dateRequest = req.body.dateRequest;
    const startDate = req.body.startDate;
    const description = req.body.description;
    const role = req.body.role;

    let proyect = new Proyect({
        name: name,
        dateRequest: dateRequest,
        startDate: startDate,
        description: description,
        role: role
    });

    proyect.save().then(obj => res.status(200).json({
        message: res.__('ok.proyects.create'),
        objs: obj
    })).catch(err => res.status(500).json({
        message:res.__('error.proyects.index'),
        objs: err
    }));
}

async function edit(req, res, next) {
    const id = req.params.id;
    const proyect = await Proyect.findOne({'_id':id});

    const name = req.body.name;
    const dateRequest = req.body.dateRequest;
    const startDate = req.body.startDate;
    const description = req.body.description;
    const role = req.body.role;

    if(name){
        proyect._name = name;
    }

    if(dateRequest){
        proyect._dateRequest = dateRequest;
    }

    if(startDate){
        proyect._startDate = startDate;
    }

    if(description){
        proyect._description = description;
    }

    if(role){
        proyect._role = role;
    }

    proyect.save().then(obj => res.status(200).json({
        message:res.__('ok.proyects.edit'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.proyects.edit'),
        objs: err
    }));
}

function replace(req, res, next) {
    const id = req.params.id;
    const name = req.body.name ? req.body.name : '';
    const dateRequest = req.body.dateRequest ? req.body.dateRequest : '';
    const startDate = req.body.startDate ? req.body.startDate : '';
    const description = req.body.description ? req.body.description : '';
    const role = req.body.role ? req.body.role : '';


    let proyect = new Object({
        _name: name,
        _dateRequest: dateRequest,
        _startDate: startDate,
        _description: description,
        _role: role
    });
    Proyect.findOneAndReplace({'_id':id}, proyect).then(obj => res.status(200).json({
        message: res.__('ok.proyects.replace'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.proyects.replace'),
        objs: err
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Proyect.remove({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.proyects.destroy'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.proyects.destroy'),
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}
