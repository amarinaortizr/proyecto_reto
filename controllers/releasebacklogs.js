const express = require('express');

const ReleaseBacklog = require('../models/releaseBacklog');

//aqui se pone 'releaseBacklogs' para los mensajes de internacionalizacion

function list(req, res, next) {
    ReleaseBacklog.find().then(objs => res.status(200).json({
        message:  res.__('ok.releaseBacklogs.list'),
        objs: objs
    })).catch(err => res.status(500).json({
        message: res.__('error.releaseBacklogs.list'),
        objs: err
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    ReleaseBacklog.findOne({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.releaseBacklogs.index'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.releaseBacklogs.index'),
        objs: err
    }));
}

function create(req, res, next) {
    const items = req.body.items;
    const userStories = req.body.userStories;
    const bugFixes = req.body.bugFixes;
    const fixes = req.body.fixes;
    const proyect = req.body.proyect;

    let releaseBacklog = new ReleaseBacklog({
        items: items,
        userStories: userStories,
        bugFixes: bugFixes,
        fixes: fixes,
        proyect: proyect
    });

    releaseBacklog.save().then(obj => res.status(200).json({
        message: res.__('ok.releaseBacklogs.create'),
        objs: obj
    })).catch(err => res.status(500).json({
        message:res.__('error.releaseBacklogs.index'),
        objs: err
    }));
}

async function edit(req, res, next) {
    const id = req.params.id;
    const releaseBacklog = await ReleaseBacklog.findOne({'_id':id});

    const items = req.body.items;
    const userStories = req.body.userStories;
    const bugFixes = req.body.bugFixes;
    const fixes = req.body.fixes;
    const proyect = req.body.proyect;

    if(items){
        releaseBacklog._items = items;
    }

    if(userStories){
        releaseBacklog._userStories = userStories;
    }

    if(bugFixes){
        releaseBacklog._bugFixes = bugFixes;
    }

    if(fixes){
        releaseBacklog._fixes = fixes;
    }

    if(proyect){
        releaseBacklog._proyect = proyect;
    }

    releaseBacklog.save().then(obj => res.status(200).json({
        message:res.__('ok.releaseBacklogs.edit'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.releaseBacklogs.edit'),
        objs: err
    }));
}

function replace(req, res, next) {
    const id = req.params.id;
    const items = req.body.items ? req.body.items : '';
    const userStories = req.body.userStories ? req.body.userStories : '';
    const bugFixes = req.body.bugFixes ? req.body.bugFixes : '';
    const fixes = req.body.fixes ? req.body.fixes : '';
    const proyect = req.body.proyect ? req.body.proyect : '';

    let releaseBacklog = new Object({
        _items: items,
        _userStories: userStories,
        _bugFixes: bugFixes,
        _fixes: fixes,
        _proyect: proyect
    });
    ReleaseBacklog.findOneAndReplace({'_id':id}, releaseBacklog).then(obj => res.status(200).json({
        message: res.__('ok.releaseBacklogs.replace'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.releaseBacklogs.replace'),
        objs: err
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    ReleaseBacklog.remove({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.releaseBacklogs.destroy'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.releaseBacklogs.destroy'),
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}
