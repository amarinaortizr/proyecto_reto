const express = require('express');

const SprintBacklog = require('../models/sprintBacklog');

//aqui se pone 'sprintBacklogs' para los mensajes de internacionalizacion

function list(req, res, next) {
    SprintBacklog.find().then(objs => res.status(200).json({
        message:  res.__('ok.sprintBacklogs.list'),
        objs: objs
    })).catch(err => res.status(500).json({
        message: res.__('error.sprintBacklogs.list'),
        objs: err
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    SprintBacklog.findOne({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.sprintBacklogs.index'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.sprintBacklogs.index'),
        objs: err
    }));
}

function create(req, res, next) {
    const items = req.body.items;
    const userStories = req.body.userStories;
    const bugFixes = req.body.bugFixes;
    const fixes = req.body.fixes;
    const proyect = req.body.proyect;

    let sprintBacklog = new SprintBacklog({
        items: items,
        userStories: userStories,
        bugFixes: bugFixes,
        fixes: fixes,
        proyect: proyect
    });

    sprintBacklog.save().then(obj => res.status(200).json({
        message: res.__('ok.sprintBacklogs.create'),
        objs: obj
    })).catch(err => res.status(500).json({
        message:res.__('error.sprintBacklogs.index'),
        objs: err
    }));
}

async function edit(req, res, next) {
    const id = req.params.id;
    const sprintBacklog = await SprintBacklog.findOne({'_id':id});

    const items = req.body.items;
    const userStories = req.body.userStories;
    const bugFixes = req.body.bugFixes;
    const fixes = req.body.fixes;
    const proyect = req.body.proyect;

    if(items){
        sprintBacklog._items = items;
    }

    if(userStories){
        sprintBacklog._userStories = userStories;
    }

    if(bugFixes){
        sprintBacklog._bugFixes = bugFixes;
    }

    if(fixes){
        sprintBacklog._fixes = fixes;
    }

    if(proyect){
        sprintBacklog._proyect = proyect;
    }

    sprintBacklog.save().then(obj => res.status(200).json({
        message:res.__('ok.sprintBacklogs.edit'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.sprintBacklogs.edit'),
        objs: err
    }));
}

function replace(req, res, next) {
    const id = req.params.id;
    const items = req.body.items ? req.body.items : '';
    const userStories = req.body.userStories ? req.body.userStories : '';
    const bugFixes = req.body.bugFixes ? req.body.bugFixes : '';
    const fixes = req.body.fixes ? req.body.fixes : '';
    const proyect = req.body.proyect ? req.body.proyect : '';

    let sprintBacklog = new Object({
        _items: items,
        _userStories: userStories,
        _bugFixes: bugFixes,
        _fixes: fixes,
        _proyect: proyect
    });
    SprintBacklog.findOneAndReplace({'_id':id}, sprintBacklog).then(obj => res.status(200).json({
        message: res.__('ok.sprintBacklogs.replace'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.sprintBacklogs.replace'),
        objs: err
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    SprintBacklog.remove({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.sprintBacklogs.destroy'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.sprintBacklogs.destroy'),
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}
