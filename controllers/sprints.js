const express = require('express');

const Sprint = require('../models/sprint');

//aqui se pone 'sprints' para los mensajes de internacionalizacion

function list(req, res, next) {
    Sprint.find().then(objs => res.status(200).json({
        message:  res.__('ok.sprints.list'),
        objs: objs
    })).catch(err => res.status(500).json({
        message: res.__('error.sprints.list'),
        objs: err
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Sprint.findOne({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.sprints.index'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.sprints.index'),
        objs: err
    }));
}

function create(req, res, next) {
    const startDate = req.body.startDate;
    const finalDate = req.body.finalDate;

    let sprint = new Sprint({
        startDate: startDate,
        finalDate: finalDate
    });

    sprint.save().then(obj => res.status(200).json({
        message: res.__('ok.sprints.create'),
        objs: obj
    })).catch(err => res.status(500).json({
        message:res.__('error.sprints.index'),
        objs: err
    }));
}

async function edit(req, res, next) {
    const id = req.params.id;
    const sprint = await Sprint.findOne({'_id':id});

    const startDate = req.body.startDate;
    const finalDate = req.body.finalDate;

    if(startDate){
        sprint._startDate = startDate;
    }

    if(finalDate){
        sprint._finalDate = finalDate;
    }

    sprint.save().then(obj => res.status(200).json({
        message:res.__('ok.sprints.edit'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.sprints.edit'),
        objs: err
    }));
}

function replace(req, res, next) {
    const id = req.params.id;
    const startDate = req.body.startDate ? req.body.startDate : '';
    const finalDate = req.body.finalDate ? req.body.finalDate : '';

    let sprint = new Object({
        _startDate: startDate,
        _finalDate: finalDate,
    });
    Sprint.findOneAndReplace({'_id':id}, sprint).then(obj => res.status(200).json({
        message: res.__('ok.sprints.replace'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.sprints.replace'),
        objs: err
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Sprint.remove({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.sprints.destroy'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.sprints.destroy'),
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}
