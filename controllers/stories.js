const express = require('express');

const Storie = require('../models/storie');

//aqui se pone 'stories' para los mensajes de internacionalizacion

function list(req, res, next) {
    Storie.find().then(objs => res.status(200).json({
        message:  res.__('ok.stories.list'),
        objs: objs
    })).catch(err => res.status(500).json({
        message: res.__('error.stories.list'),
        objs: err
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Storie.findOne({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.stories.index'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.stories.index'),
        objs: err
    }));
}

function create(req, res, next) {
    const fibId = req.body.fibId;
    const data = req.body.data;
    const reverseData = req.body.reverseData;

    let storie = new Storie({
        fibId: fibId,
        data: data,
        reverseData: reverseData
    });

    storie.save().then(obj => res.status(200).json({
        message: res.__('ok.stories.create'),
        objs: obj
    })).catch(err => res.status(500).json({
        message:res.__('error.stories.index'),
        objs: err
    }));
}

async function edit(req, res, next) {
    const id = req.params.id;
    const storie = await Storie.findOne({'_id':id});

    const fibId = req.body.fibId;
    const data = req.body.data;
    const reverseData = req.body.reverseData;

    if(fibId){
        storie._fibId = fibId;
    }

    if(data){
        storie._data = data;
    }

    if(reverseData){
        storie._reverseData = reverseData;
    }

    storie.save().then(obj => res.status(200).json({
        message:res.__('ok.stories.edit'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.stories.edit'),
        objs: err
    }));
}

function replace(req, res, next) {
    const id = req.params.id;
    const fibId = req.body.fibId ? req.body.fibId : '';
    const data = req.body.data ? req.body.data : '';
    const reverseData = req.body.reverseData ? req.body.reverseData : '';

    let storie = new Object({
        _id: id,
        _data: data,
        _reverseData: reverseData
    });
    Storie.findOneAndReplace({'_id':id}, storie).then(obj => res.status(200).json({
        message: res.__('ok.stories.replace'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.stories.replace'),
        objs: err
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Storie.remove({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.stories.destroy'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.stories.destroy'),
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}
