const express = require('express');

const User = require('../models/user');

//aqui se pone 'users' para los mensajes de internacionalizacion

function list(req, res, next) {
    User.find().then(objs => res.status(200).json({
        message:  res.__('ok.users.list'),
        objs: objs
    })).catch(err => res.status(500).json({
        message: res.__('error.users.list'),
        objs: err
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    User.findOne({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.users.index'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.users.index'),
        objs: err
    }));
}

function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    const birthDate = req.body.birthDate;
    const curp = req.body.curp;
    const rfc = req.body.rfc;
    const address = req.body.address;
    const skills = req.body.skills;
    const password = req.body.password;
    const salt = req.body.salt;
    const permissions = req.body.permissions;

    let user = new User({
        name: name,
        lastName: lastName,
        birthDate: birthDate,
        curp: curp,
        rfc: rfc,
        address: address,
        skills: skills,
        password: password,
        salt: salt,
        permissions: permissions
    });

    user.save().then(obj => res.status(200).json({
        message: res.__('ok.users.create'),
        objs: obj
    })).catch(err => res.status(500).json({
        message:res.__('error.users.index'),
        objs: err
    }));
}

async function edit(req, res, next) {
    const id = req.params.id;
    const user = await User.findOne({'_id':id});

    const name = req.body.name;
    const lastName = req.body.lastName;
    const birthDate = req.body.birthDate;
    const curp = req.body.curp;
    const rfc = req.body.rfc;
    const address = req.body.address;
    const skills = req.body.skills;
    const password = req.body.password;
    const salt = req.body.salt;
    const permissions = req.body.permissions;

    if(name){
        user._name = name;
    }

    if(lastName){
      user._lastName = lastName;
    }

    if(birthDate){
        user._birthDate = birthDate;
    }

    if(curp){
        user._curp = curp;
    }

    if(rfc){
        user._rfc = rfc;
    }

    if(address){
        user._address = address;
    }

    if(skills){
        user._skills = skills;
    }

    if(password){
      user._password = password;
    }

    if(salt){
      user._salt = salt;
    }

    if(permissions){
        user._permissions = permissions;
}

    user.save().then(obj => res.status(200).json({
        message:res.__('ok.users.edit'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.users.edit'),
        objs: err
    }));
}

function replace(req, res, next) {
    const id = req.params.id;
    const name = req.body.name ? req.body.name : '';
    const lastName = req.body.lastName ? req.body.lastName : '';
    const birthDate = req.body.birthDate ? req.body.birthDate : '';
    const curp = req.body.curp ? req.body.curp : '';
    const rfc = req.body.rfc ? req.body.rfc : '';
    const address = req.body.address ? req.body.address : '';
    const skills = req.body.skills ? req.body.skills : '';
    const password = req.body.password ? req.body.password : '';
    const salt = req.body.salt ? req.body.salt : '';
    const permissions = req.body.permissions ? req.body.permissions : '';

    let user = new Object({
        _name: name,
        _lastName: lastName,
        _birthDate: birthDate,
        _curp: curp,
        _rfc: rfc,
        _address: address,
        _skills: skills,
        _password: password,
        _salt: salt,
        _permissions: permissions
    });
    User.findOneAndReplace({'_id':id}, user).then(obj => res.status(200).json({
        message: res.__('ok.users.replace'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.users.replace'),
        objs: err
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    User.remove({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.users.destroy'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.users.destroy'),
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}
