FROM node
LABEL authors="Luis Izquierdo, Axel Rivas, Marina Ortiz"
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD npm start
