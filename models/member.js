const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
  _fullName:String,
  _birthDate:Date,
  _curp:String,
  _rfc:String,
  _address:String,
  _skills:[],
  _permissions:[String]
});

class Member {

    constructor(fullName, birthDate, curp, rfc, address, skills, permission) {
        this._fullName = fullName;
        this._birthDate = birthDate;
        this._curp = curp;
        this._rfc = rfc;
        this._address = address;
        this._skills = skills;
        this._permissions = permission;
    }

    get fullName(){
      return this._fullName;
    }

    set fullName(v){
      this._fullName=v;
    }

    get birthDate(){
      return this._birthDate;
    }

    set birthDate(v){
      this._birthDate=v;
    }

    get curp(){
      return this._curp;
    }

    set curp(v){
      this._curp=v;
    }

    get rfc(){
      return this._rfc;
    }

    set rfc(v){
      this._rfc=v;
    }

    get address(){
      return this._address;
    }

    set address(v){
      this._address=v;
    }

    get skills(){
      return this._skills;
    }

    set skills(v){
      this._skills=v;
    }

    get permission(){
      return this._permissions;
    }

    set permission(v){
      this._permissions=v;
    }

}

schema.loadClass(Member);
module.exports = mongoose.model('Member', schema);
