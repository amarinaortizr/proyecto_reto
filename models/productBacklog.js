const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
  _features:String,
  _requirements:String,
  _enhacements:String,
  _fixes:String,
  _proyect:String
});


class ProductBacklog {

    constructor(features, requirements, enhacements, fixes, proyect) {
            this._features = features;
            this._requirements = requirements;
            this._enhacements = enhacements;
            this._fixes = fixes;
            this._proyect = proyect;
    }

    get features() {
        return this._features;
    }

    set features(v) {
        this._features = v;
    }

    get requirements() {
        return this._requirements;
    }

    set requirements(v) {
        this._requirements = v;
    }

    get enhacements() {
        return this._enhacements;
    }

    set enhacements(v) {
        this._enhacements = v;
    }

    get fixes() {
        return this._fixes;
    }

    set fixes(v) {
        this._fixes = v;
    }

    get proyect() {
        return this._proyect;
    }

    set proyect(v) {
        this._proyect = v;
    }

}

schema.loadClass(ProductBacklog);
module.exports = mongoose.model('ProductBacklog', schema);
