const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
  _name:String,
  _dateRequest:String,
  _startDate:Date,
  _description:String,
  _role: String
});

class Proyect {

    constructor(name, dateRequest, startDate, description, role) {
            this._name = name;
            this._dateRequest = dateRequest;
            this._startDate = startDate;
            this._description = description;
            this._role = role;
    }

    get name() {
        return this._name;
    }

    set name(v) {
        this._name = v;
    }

    get dateRequest() {
        return this._dateRequest;
    }

    set dateRequest(v) {
        this._dateRequest = v;
    }

    get startDate() {
        return this._startDate;
    }

    set startDate(v) {
        this._startDate = v;
    }

    get description() {
        return this._description;
    }

    set description(v) {
        this._description = v;
    }

    get role() {
        return this._role;
    }

    set role(v) {
        this._role = v;
    }

}

schema.loadClass(Proyect);
module.exports = mongoose.model('Proyect', schema);
