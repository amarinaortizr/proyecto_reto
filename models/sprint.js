const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
  _startDate:String,
  _finalDate:String
});

class Sprint {

    constructor(startDate, finalDate) {
            this._startDate = startDate;
            this._finalDate = finalDate;
    }

    get startDate() {
        return this._startDate;
    }

    set startDate(v) {
        this._startDate = v;
    }

    get finalDate() {
        return this._finalDate;
    }

    set finalDate(v) {
        this._finalDate = v;
    }



}

schema.loadClass(Sprint);
module.exports = mongoose.model('Sprint', schema);
