const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
  _items:String,
  _userStories:String,
  _bugFixes:Date,
  _fixes:String,
  _proyect:String
});

class SprintBacklog {

    constructor(items, userStories, bugFixes, fixes, proyect) {
            this._items = items;
            this._userStories = userStories;
            this._bugFixes = bugFixes;
            this._fixes = fixes;
            this._proyect = proyect;
    }

    get items() {
        return this._items;
    }

    set items(v) {
        this._items = v;
    }

    get userStories() {
        return this._userStories;
    }

    set userStories(v) {
        this._userStories = v;
    }

    get bugFixes() {
        return this._bugFixes;
    }

    set bugFixes(v) {
        this._bugFixes = v;
    }

    get fixes() {
        return this._fixes;
    }

    set fixes(v) {
        this._fixes = v;
    }

    get proyect() {
        return this._proyect;
    }

    set proyect(v) {
        this._proyect = v;
    }

}

schema.loadClass(SprintBacklog);
module.exports = mongoose.model('SprintBacklog', schema);
