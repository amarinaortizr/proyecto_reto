const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
  _fibId: Number,
  _data: Array,
  _reverseData: Array
});

class Storie {

    constructor(fibId, data, reverseData) {
            this._fibId = fibId;
            this._data = data;
            this._reverseData = reverseData;
    }

    get fibId() {
        return this._fibId;
    }

    set fibId(v) {
        this._fibId = v;
    }

    get data() {
        return this._data;
    }

    set data(v) {
        this._data = v;
    }

    get reverseData() {
        return this._reverseData;
    }

    set reverseData(v) {
        this._reverseData = v;
    }

}

schema.loadClass(Storie);
module.exports = mongoose.model('Storie', schema);
