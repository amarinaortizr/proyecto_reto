const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = mongoose.Schema({
  _name:String,
  _lastName:String,
  _birthDate:Date,
  _curp:String,
  _rfc:String,
  _address:String,
  _skills:[],
  _password:String,
  _salt:String,
  _permissions:[String]
});

class User {
  constructor(name, lastName, birthDate, curp, rfc, address, skills, password, salt, permissions){
    this._name = name;
    this._lastName = lastName;
    this._birthDate = birthDate;
    this._curp = curp;
    this._rfc = rfc;
    this._address = address;
    this._skills = skills;
    this._password = password;
    this._salt = salt;
    this._permissions = permissions;
  }

  get name(){
    return this._name;
  }

  set name(v){
    this._name = v;
  }

  get lastName(){
    return this._lastName;
  }

  set lastName(v){
    this._lastName = v;
  }

  get birthDate(){
    return this._birthDate;
  }

  set birthDate(v){
    this._birthDate = v;
  }

  get curp(){
    return this._curp;
  }

  set curp(v){
    this._curp = v;
  }

  get rfc(){
    return this._rfc;
  }

  set rfc(v){
    this._rfc = v;
  }

  get address(){
    return this._address;
  }

  set address(v){
    this._address = v;
  }

  get skills(){
    return this._skills;
  }

  set skills(v){
    this._skills = v;
  }

  get password(){
    return this._password;
  }

  set password(v){
    this._password = v;
  }

  get salt(){
    return this._salt;
  }

  set salt(v){
    this._salt = v;
  }

  get permissions(){
    return this._permissions;
  }

  set permissions(v){
    this._permissions = v;
  }
}

schema.loadClass(User);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('User', schema);
